#!/bin/sh

L=$1
nts=$2
bfs=$3
bas=$4
ntherm=$5
nskip=$6
nmeas=$7

scandir=data/scan-$$
mkdir -p $scandir
for NT in $nts; do
	for bf in $bfs; do
		for ba in $bas; do
			echo "./ym $L $NT $bf $ba $ntherm $nskip $nmeas > $scandir/dat,$L,$NT,$bf,$ba"
		done
	done
done
