FFLAGS = -fimplicit-none -Ofast -Wall
LDLIBS = -llapack
#FFLAGS += -fdefault-real-8
#FFLAGS += -ffast-math
#FFLAGS += -march=native
#FFLAGS += -fsanitize=address -O0 -g
#FFLAGS += -O0 -g
#FFLAGS += -pg
#FFLAGS += -fcheck=all

all: ym

%: %.f90
	gfortran ${FFLAGS} -o $@ $< ${LDLIBS}

clean:
	${RM} ym
