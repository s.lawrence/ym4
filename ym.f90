program ym

    real, parameter :: pi = 3.141592653589793238462643383279502884197169399375105820974

    integer, parameter :: Nc = 3

    integer, parameter :: pool_size = 500

    integer, parameter :: nrounds = 5

    character(len=50) :: Lstr, NTstr, betafstr, betaastr
    character(len=50) :: nthermstr, nskipstr, nmeasstr, seedstr, delstr
    integer :: L, NT
    real :: beta_f, beta_a, del
    integer :: ntherm, nskip, nmeas
    integer :: seedval

    complex, dimension(Nc,Nc,pool_size) :: pool

    if (command_argument_count() /= 8 .and. command_argument_count() /= 9) then
        stop "L NT beta_f beta_a ntherm nskip nmeas seed [del]"
    end if

    call get_command_argument(1, Lstr)
    call get_command_argument(2, NTstr)
    call get_command_argument(3, betafstr)
    call get_command_argument(4, betaastr)
    call get_command_argument(5, nthermstr)
    call get_command_argument(6, nskipstr)
    call get_command_argument(7, nmeasstr)
    call get_command_argument(8, seedstr)

    delstr = "0.3"
    if (command_argument_count() == 9) then
        call get_command_argument(9, delstr)
    end if

    read (Lstr,*) L
    read (NTstr,*) NT
    read (betafstr,*) beta_f
    read (betaastr,*) beta_a
    read (nthermstr,*) ntherm
    read (nskipstr,*) nskip
    read (nmeasstr,*) nmeas
    read (seedstr,*) seedval
    read (delstr,*) del

    block
        integer, allocatable :: seed(:)
        integer :: sz
        call random_seed(size=sz)
        allocate(seed(sz))
        seed = seedval
        call random_seed(put=seed)
        deallocate(seed)
    end block

    call prepare_pool
    call main

contains

subroutine main
    complex, allocatable, dimension(:,:,:,:,:,:,:) :: U
    integer i,j
    integer n

    allocate(U(Nc,Nc,4,L,L,L,NT))
    U = 0
    do n = 1,Nc
        U(n,n,:,:,:,:,:) = 1
    end do

    ! Disorder the whole lattice.
!    block
!        integer :: x,y,z,t,d
!        do x = 1,L
!            do y = 1,L
!                do z = 1,L
!                    do t = 1,NT
!                        do d = 1,4
!                            call random_pool(U(:,:,d,x,y,z,t))
!                        end do
!                    end do
!                end do
!            end do
!        end do
!    end block

    do i = 1,ntherm
        call sweep(U)
    end do

    do i = 1,nmeas
        do j = 1,nskip
            call sweep(U)
        end do
        call measure(U)

        if (mod(i,10) == 0) then
            call reunitarize(U)
        end if
    end do

    deallocate(U)
end subroutine

subroutine sweep(U)
    complex, intent(inout), dimension(Nc,Nc,4,L,L,L,NT) :: U
    complex, dimension(Nc,Nc) :: Up
    integer :: x, y, z, t, d
    integer :: r
    complex, dimension(Nc,Nc,6) :: A
    real :: S, Sp, uniform
    complex :: tr
    real :: tra

    do x = 1,L
        do y = 1,L
            do z = 1,L
                do t = 1,NT
                    do d = 1,4
                        A = staples(U,x,y,z,t,d)
                        tr = trace_staples_fundamental(A,U(:,:,d,x,y,z,t))
                        tra = trace_staples_adjoint(A,U(:,:,d,x,y,z,t))
                        S = -beta_f*real(tr)/Nc - beta_a*tra/(Nc*Nc)
                        do r = 1,nrounds
                            call random_pool(Up)
                            Up(:,:) = matmul(Up(:,:), U(:,:,d,x,y,z,t))
                            tr = trace_staples_fundamental(A,Up)
                            tra = trace_staples_adjoint(A,Up)
                            Sp = -beta_f*real(tr)/Nc - beta_a*tra/(Nc*Nc)
                            call random_number(uniform)
                            if (uniform < exp(S-Sp)) then
                                U(:,:,d,x,y,z,t) = Up
                                S = Sp
                            end if
                        end do
                    end do
                end do
            end do
        end do
    end do
end subroutine

function trace_staples_fundamental(A,U) result(tr)
    complex, intent(in), dimension(Nc,Nc,6) :: A
    complex, intent(in), dimension(Nc,Nc) :: U
    integer i
    complex tr
    tr = 0
    do i = 1,6
        tr = tr + trace_prod(A(:,:,i),U)
    end do
end function

function trace_staples_adjoint(A, U) result(tr)
    complex, intent(in), dimension(Nc,Nc,6) :: A
    complex, intent(in), dimension(Nc,Nc) :: U
    integer i
    complex trf
    real tr
    tr = 0
    do i = 1,6
        trf = trace_prod(A(:,:,i),U)
        tr = tr + real(trf*conjg(trf))
    end do
end function

function staples(U,x_,y_,z_,t_,d) result(A)
    complex, intent(in), dimension(Nc,Nc,4,L,L,L,NT) :: U
    complex, dimension(Nc,Nc,6) :: A
    integer, intent(in) :: x_, y_, z_, t_, d
    integer :: x, y, z, t
    integer :: i, dp, j

    x = x_
    y = y_
    z = z_
    t = t_

    A = 0
    do i = 1,Nc
        A(i,i,:) = 1
    end do

    j = 0
    do dp = 1,4
        if (dp == d) then
            cycle
        end if

        ! Two staples for each direction other than d.

        j = j+1
        call step(x,y,z,t,d)
        A(:,:,j) = matmul(A(:,:,j),U(:,:,dp,x,y,z,t))
        call step(x,y,z,t,dp)
        call step(x,y,z,t,-d)
        A(:,:,j) = matmul(A(:,:,j),adjoint(U(:,:,d,x,y,z,t)))
        call step(x,y,z,t,-dp)
        A(:,:,j) = matmul(A(:,:,j),adjoint(U(:,:,dp,x,y,z,t)))

        j = j+1
        call step(x,y,z,t,d)
        call step(x,y,z,t,-dp)
        A(:,:,j) = matmul(A(:,:,j),adjoint(U(:,:,dp,x,y,z,t)))
        call step(x,y,z,t,-d)
        A(:,:,j) = matmul(A(:,:,j),adjoint(U(:,:,d,x,y,z,t)))
        A(:,:,j) = matmul(A(:,:,j),U(:,:,dp,x,y,z,t))
        call step(x,y,z,t,dp)
    end do
end function

function plaquette(U,x_,y_,z_,t_,d,dp) result(P)
    complex, intent(in), dimension(Nc,Nc,4,L,L,L,NT) :: U
    integer, intent(in) :: x_, y_, z_, t_, d, dp
    complex, dimension(Nc,Nc) :: P
    integer :: x, y, z, t, i

    x = x_
    y = y_
    z = z_
    t = t_

    P = 0
    do i = 1,Nc
        P(i,i) = 1
    end do

    P = matmul(P, U(:,:,d,x,y,z,t))
    call step(x,y,z,t,d)
    P = matmul(P, U(:,:,dp,x,y,z,t))
    call step(x,y,z,t,dp)
    call step(x,y,z,t,-d)
    P = matmul(P, adjoint(U(:,:,d,x,y,z,t)))
    call step(x,y,z,t,-dp)
    P = matmul(P, adjoint(U(:,:,dp,x,y,z,t)))
end function

function polyakov(U,x,y,z) result(P)
    complex, intent(in), dimension(Nc,Nc,4,L,L,L,NT) :: U
    integer, intent(in) :: x,y,z
    complex, dimension(Nc,Nc) :: P
    integer :: t, i

    P = 0
    do i = 1,Nc
        P(i,i) = 1
    end do

    do t = 1,NT
        P = matmul(P, U(:,:,4,x,y,z,t))
    end do
end function

subroutine step(x,y,z,t,d)
    integer, intent(inout) :: x, y, z, t
    integer, intent(in) :: d
    select case (d)
    case (1)
        x = mod(x,L)+1
    case (2)
        y = mod(y,L)+1
    case (3)
        z = mod(z,L)+1
    case (4)
        t = mod(t,NT)+1
    case (-1)
        x = mod(x-2+L,L)+1
    case (-2)
        y = mod(y-2+L,L)+1
    case (-3)
        z = mod(z-2+L,L)+1
    case (-4)
        t = mod(t-2+NT,NT)+1
    case default
        stop "Bad step direction"
    end select
end subroutine

subroutine reunitarize(U)
    complex, intent(inout), dimension(Nc,Nc,4,L,L,L,NT) :: U
    integer :: x, y, z, t, d
    
    do x = 1,L
        do y = 1,L
            do z = 1,L
                do t = 1,NT
                    do d = 1,4
                        call unitarize(U(:,:,d,x,y,z,t))
                    end do
                end do
            end do
        end do
    end do
end subroutine

subroutine measure(U)
    complex, intent(in), dimension(Nc,Nc,4,L,L,L,NT) :: U
    complex, dimension(Nc,Nc) :: P
    integer :: d, x, y, z, t, dt, dx
    real :: tr_f, tr_a, tr_ft, tr_at, polf, pola
    real :: tr_i, det_i
    real, dimension(NT) :: tr_f_t, tr_a_t
    real, dimension(NT) :: cor_f, cor_a
    complex, dimension(L,L,L) :: pols_f, pols_a
    real, dimension(L) :: cpol_f, cpol_a

    tr_f = 0
    tr_a = 0
    tr_ft = 0
    tr_at = 0
    polf = 0
    pola = 0

    ! Correlators
    tr_f_t = 0
    tr_a_t = 0
    do t = 1,NT
        do x = 1,L
            do y = 1,L
                do z = 1,L
                    ! Spatial plaquettes
                    P = plaquette(U,x,y,z,t,1,2)
                    tr_f_t(t) = tr_f_t(t) + real(trace(P)) / (3*L*L*L)
                    tr_a_t(t) = tr_a_t(t) + (abs(trace(P))**2 - 1) / (3*L*L*L)
                    P = plaquette(U,x,y,z,t,1,3)
                    tr_f_t(t) = tr_f_t(t) + real(trace(P)) / (3*L*L*L)
                    tr_a_t(t) = tr_a_t(t) + (abs(trace(P))**2 - 1) / (3*L*L*L)
                    P = plaquette(U,x,y,z,t,2,3)
                    tr_f_t(t) = tr_f_t(t) + real(trace(P)) / (3*L*L*L)
                    tr_a_t(t) = tr_a_t(t) + (abs(trace(P))**2 - 1) / (3*L*L*L)

                end do
            end do
        end do
    end do

    do dt = 0,NT-1
        cor_f(dt+1) = sum(tr_f_t * cshift(tr_f_t, dt))/NT
        cor_a(dt+1) = sum(tr_a_t * cshift(tr_a_t, dt))/NT
    end do

    ! Spatial plaquettes
    tr_f = sum(tr_f_t)/NT
    tr_a = sum(tr_a_t)/NT

    ! Polyakov loops
    do x = 1,L
        do y = 1,L
            do z = 1,L
                P = polyakov(U,x,y,z)
                pols_f(x,y,z) = trace(P)
                pols_a(x,y,z) = abs(trace(P))**2 - 1
            end do
        end do
    end do
    polf = sum(real(pols_f)) / (L*L*L)
    pola = sum(real(pols_a)) / (L*L*L)

    ! Heavy quark potential
    cpol_f = 0
    cpol_a = 0
    do dx = 0,L-1
        do d = 1,3
            cpol_f(dx+1) = real(sum(conjg(pols_f) * cshift(pols_f, dx, d)))/(L*L*L)
            cpol_a(dx+1) = real(sum(conjg(pols_a) * cshift(pols_a, dx, d)))/(L*L*L)
        end do
    end do

    ! Temporal plaquettes
    do x = 1,L
        do y = 1,L
            do z = 1,L
                do t = 1,NT
                    P = plaquette(U,x,y,z,t,1,4)
                    tr_ft = tr_ft + real(trace(P)) / (3*L*L*L*NT)
                    tr_at = tr_at + (abs(trace(P))**2 - 1) / (3*L*L*L*NT)
                    P = plaquette(U,x,y,z,t,2,4)
                    tr_ft = tr_ft + real(trace(P)) / (3*L*L*L*NT)
                    tr_at = tr_at + (abs(trace(P))**2 - 1) / (3*L*L*L*NT)
                    P = plaquette(U,x,y,z,t,3,4)
                    tr_ft = tr_ft + real(trace(P)) / (3*L*L*L*NT)
                    tr_at = tr_at + (abs(trace(P))**2 - 1) / (3*L*L*L*NT)
                end do
            end do
        end do
    end do

    ! Debugging observables
    tr_i = abs(Nc-trace_prod(adjoint(U(:,:,1,1,1,1,1)),U(:,:,1,1,1,1,1)))
    det_i = abs(1.-determinant(U(:,:,1,1,1,1,1)))

    write (*,"(F18.10)",advance="no") tr_f
    write (*,"(F18.10)",advance="no") tr_a
    write (*,"(F18.10)",advance="no") tr_ft
    write (*,"(F18.10)",advance="no") tr_at
    do dx = 0,L-1
        write (*,"(F18.10)",advance="no") cpol_f(dx+1)
    end do
    do dx = 0,L-1
        write (*,"(F18.10)",advance="no") cpol_a(dx+1)
    end do
    do dt = 0,NT-1
        write (*,"(F18.10)",advance="no") cor_f(dt+1)
    end do
    do dt = 0,NT-1
        write (*,"(F18.10)",advance="no") cor_a(dt+1)
    end do
    print *, polf, pola, tr_i, det_i
    !write (*,"()")
end subroutine

function determinant(U) result(r)
    use iso_fortran_env, only : REAL32
    integer, parameter :: prec = REAL32

    complex, intent(in), dimension(Nc,Nc) :: U
    complex(kind=prec), dimension(Nc,Nc) :: A
    complex(kind=prec), dimension(Nc) :: w
    complex(kind=prec), dimension(1) :: ign
    complex(kind=prec), dimension(Nc*Nc) :: work
    real(kind=prec), dimension(2*Nc) :: rwork
    integer :: info
    complex(kind=prec) :: r

    A = U
    call cgeev('N','N',Nc,A,Nc,w,ign,1,ign,1,work,size(work),rwork,info)
    if (info /= 0) then
        stop "bad cgeev"
    end if
    r = product(w)
end function

subroutine prepare_pool
    integer :: n
    do n = 1,pool_size
        call random_special_unitary(pool(:,:,n))
    end do
end subroutine

subroutine random_pool(U)
    complex, dimension(Nc,Nc), intent(out) :: U
    real :: x
    call random_number(x)
    U = pool(:,:,1+floor(x*pool_size))
    call random_number(x)
    if (x < 0.5) then
        U = adjoint(U)
    end if
end subroutine

subroutine random_normals(x, y)
    real, intent(out) :: x, y
    real :: a, b
    call random_number(a)
    call random_number(b)
    a = a * 2 * pi
    b = -log(b)
    x = b*cos(a)
    y = b*sin(a)
end subroutine

subroutine random_normalz(z)
    complex, intent(out) :: z
    real :: x,y
    call random_normals(x,y)
    z = x + (0,1)*y
end subroutine

subroutine random_special_unitary(U)
    complex, dimension(Nc,Nc), intent(out) :: U
    complex, dimension(Nc,Nc) :: H, Hk
    integer :: i,j,k
    complex :: tr

    ! First make a random Hermitian matrix.
    H = 0
    do i = 1,Nc
        do j = i,Nc
            call random_normalz(H(i,j))
            if (i == j) then
                H(i,j) = real(H(i,j))
            else
                H(j,i) = conjg(H(i,j))
            end if
        end do
    end do

    ! Scale down.
    H = H * del

    ! Force to be traceless.
    tr = 0
    do i = 1,Nc
        tr = tr + H(i,i)
    end do
    do i = 1,Nc
        H(i,i) = H(i,i) - tr/Nc
    end do

    H = H * (0,1)

    ! Exponentiate.
    U = 0
    Hk = 0
    do i = 1,Nc
        Hk(i,i) = 1.
    end do
    k = 0
    do while (sum(abs(Hk)) > 1e-14 .and. k < 20)
        U = U + Hk
        k = k+1
        Hk = matmul(Hk, H) / k
    end do

    ! Unitarize
    call unitarize(U)

    ! Sometimes, multiply by an element of the center.
    block
        real :: x
        call random_number(x)
        if (x < 0.5) then
            U = U * exp((0,1) * 2 * pi / Nc)
        end if
    end block
end subroutine

subroutine unitarize(U)
    complex, dimension(Nc,Nc), intent(inout) :: U
    integer :: i,j
    complex :: x

    do i = 1,Nc
        do j = 1,i-1
            x = sum(conjg(U(:,j)) * U(:,i))
            U(:,i) = U(:,i) - x * U(:,j)
        end do
        x = sum(conjg(U(:,i)) * U(:,i))
        U(:,i) = U(:,i) / sqrt(x)
    end do

    x = determinant(U)
    U = U / x**(1./Nc)
end subroutine

function adjoint(M) result(A)
    complex, dimension(:,:), intent(in) :: M
    complex, dimension(size(M,1),size(M,2)) :: A
    A = transpose(conjg(M))
end function

function trace(A) result(r)
    complex, dimension(:,:), intent(in) :: A
    complex :: r
    integer :: i

    r = 0
    do i = 1,size(A,1)
        r = r + A(i,i)
    end do
end function

function trace_prod(A,B) result(r)
    complex, dimension(:,:), intent(in) :: A,B
    complex :: r
    integer :: i,j

    r = 0
    do i=1,size(A,1)
        do j=1,size(B,1)
            r = r + A(i,j) * B(j,i)
        end do
    end do
end function

end program
