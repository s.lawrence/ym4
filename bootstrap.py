#!/usr/bin/env python3

import sys
from numpy import *

def bootstrap(xs, ws=None, N=100, B=50):
    if ws is None:
        ws = xs*0 + 1
    # Block
    x, w = [], []
    for i in range(int(len(xs)/B)):
        x.append(sum(xs[i*B:i*B+B]*ws[i*B:i*B+B])/sum(ws[i*B:i*B+B]))
        w.append(sum(ws[i*B:i*B+B]))
    x = array(x)
    w = array(w)
    # Regular bootstrap
    y = x * w
    m = (sum(y) / sum(w)).real
    ms = []
    for n in range(N):
        s = random.choice(range(len(x)), len(x))
        ms.append((sum(y[s]) / sum(w[s])).real)
    return m, std(ms)

dat = array([[complex(x) for x in l.split()] for l in sys.stdin.readlines() if l[0] != '#'])

B = 100
if len(sys.argv) == 2:
    B = int(sys.argv[1])

for k in range(dat.shape[1]):
    print(*bootstrap(dat[:,k], B=B))
